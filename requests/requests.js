import {addDoc, collection, doc, getDocs, query, updateDoc, where} from 'firebase/firestore'
import {createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut} from 'firebase/auth';
import {db, auth} from '../firebase-config.js';

const bookmarksRef = collection(db, 'bookmarks');
const tagsRef = collection(db, 'tags');

/**
 * Fonction permettant de récupérer tous les tags
 *
 * @returns
 */
export const getTags = async (uid) => {
  const q = query(tagsRef,  where('uid', '==', uid));
  try {
    const querySnapshot = await getDocs(q);
    return prepareDataFromQuerySnapshot(querySnapshot);
  } catch (e) {
    console.error(e);
    throw e;
  }
}

/**
 * Fonction permettant d'ajouter un tag
 *
 * @param {*} data
 */
export const addTags = async (data) => {
  try {
    await addDoc(tagsRef, data);
  } catch (e) {
    console.error("Error adding document: ", e);
  }
}

/**
 * Fonction permettant de récupérer tous les favoris
 *
 * @returns
 */
export const getBookmarks = async (uid) => {
  const q = query(bookmarksRef, where('uid', '==', uid));
  try {
    const querySnapshot = await getDocs(q);
    return prepareDataFromQuerySnapshot(querySnapshot);
  } catch (e) {
    console.error(e);
    throw e;
  }
}

/**
 * Fonction permettant de récupérer un favori via l'URL
 *
 * @param {*} url
 * @param uid
 * @returns
 */
export const getBookmarksByLink = async (url, uid) => {
  const q = query(bookmarksRef, where('url', '==', url), where('uid', '==', uid));
  try {
    const querySnapshot = await getDocs(q);
    return prepareDataFromQuerySnapshot(querySnapshot);
  } catch (e) {
    console.error(e);
    throw e;
  }

}

/**
 * Fonction permettant d'ajouter un favori
 *
 * @param {*} data
 */
export const addBookmark = async (data) => {
  try {
    await addDoc(bookmarksRef, data);
  } catch (e) {
    console.error("Error adding document: ", e);
  }
}

/**
 * Fonction permettant de mettre à jour un marque page
 *
 * @param {*} id
 * @param {*} data
 */
export const updateBookmark = async (id, data) => {
  try {
    const documentRef = doc(db, 'bookmarks', id);
    await updateDoc(documentRef, data);
  } catch (e) {
    console.error(e);
    throw e;
  }

}

/**
 *
 * @param mail
 * @param password
 * @returns {Promise<User>}
 */
export const signup = async (mail, password) => {
  try {
    let res = await createUserWithEmailAndPassword(auth, mail, password);
    return res.user;
  } catch (e) {
    throw e;
  }
}

/**
 *
 * @param mail
 * @param password
 * @returns {Promise<User>}
 */
export const signin = async (mail, password) => {
  try {
    let res = await signInWithEmailAndPassword(auth, mail, password);
    if(res.user) {
      return res.user;
    }
  } catch (e) {
    throw e;
  }
}

/**
 *
 * @returns {Promise<void>}
 */
export const signout = async () => {
  try {
    await signOut(auth);
  } catch (e) {
    throw e;
  }
}


/**
 * Fonction permettant de préparer les données
 *
 * @param querySnapshot
 * @returns {*[]}
 */
const prepareDataFromQuerySnapshot = (querySnapshot) => {
  let tmp = [];
  querySnapshot.forEach((doc) => {
    tmp.push({...doc.data(), doc_id: doc.id})
  });
  return tmp;
}