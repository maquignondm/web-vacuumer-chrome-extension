import {
  addBookmark,
  addTags,
  getBookmarks,
  getBookmarksByLink,
  getTags,
  signin, signout,
  updateBookmark
} from "./requests/requests.js";

import { getAuth, onAuthStateChanged } from "firebase/auth";

const auth = getAuth();

document.addEventListener('DOMContentLoaded', function() {
  const applyButton = document.querySelector('#apply-button');
  const signinButton = document.querySelector('#signin-button');
  const targetMail = document.querySelector('#target-mail');
  const targetPassword = document.querySelector('#target-password');
  const targetUrl = document.querySelector('#target-url');
  const targetTitle = document.querySelector('#target-title');
  const targetFavicon = document.querySelector('#target-favicon');
  const targetTags = document.querySelector('#target-tags');
  const loaderContainer = document.querySelector('#loader-container')
  const formContainer = document.querySelector('#form-container');
  const endContainer = document.querySelector('#end-container');
  const listContainer = document.querySelector('#list-container');
  const signinContainer = document.querySelector('#signin-container');
  const listLink = document.querySelector('#list-link');
  const logoutLink = document.querySelector('#logout-link');
  const addLink = document.querySelector('#add-link');
  const tagsInputAutocomplete = document.querySelector('#tags-autocomplete');

  let currentTags = [];
  let tags = [];
  let currentDocId = null;
  let uid = null;

  /**
   * Récupére les tags
   * Récupére le favori si il existe et pré-rempli le formulaire sinon
   * Récupère la tab active et pré-rempli le formulaire
   *
   * @returns
   */
  const init = async () => {
    let queryOptions = { active: true };
    let [tab] = await chrome.tabs.query(queryOptions);
    targetUrl.value = tab.url;
    targetTitle.value = tab.title;
    targetFavicon.src = tab.favIconUrl;
    displayActivePage('form');
    await onAuthStateChanged(auth, async (user) => {
      if (user) {
        uid = user.uid;
        document.addEventListener('keydown', (e) => (e.key === 'Enter' ? addTagToInput(targetTags.value.trim(), true) : null))
        tags = (await getTags(uid)).map(t => t.label);
        let bookmark = await getBookmarksByLink(tab.url, uid);
        if(bookmark.length > 0) {
          targetUrl.value = bookmark[0].url;
          targetTitle.value = bookmark[0].title;
          targetFavicon.src = bookmark[0].favicon;
          currentDocId = bookmark[0].doc_id;
          bookmark[0].tags.forEach(tag => addTagToInput(tag))
          targetTags.focus();
        }
      } else {
        displayActivePage('signin');
      }
    });
  }

  /**
   * Supprime un tag du DOM et du tableau tags
   *
   * @param {*} tag
   * @param {*} value
   */
  const removeTag = (tag, value) => {
    currentTags = currentTags.filter(t => t !== value);
    tag.remove();
  }

  /**
   * Permet d'ajouter un tag dans le DOM et dans le tableau tags
   *
   * @param value
   * @param insertTag
   * @returns {Promise<void>}
   */
  const addTagToInput = async (value, insertTag = false) => {
    if(insertTag && !tags.includes(value) && value.length > 0) await addTags({label: value, uid});
    let newTag = document.createElement('span');
    newTag.classList.add('tag');
    let closeBtn = document.createElement('span');
    closeBtn.classList.add('tag-close-btn');
    closeBtn.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512"><path d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM175 175c9.4-9.4 24.6-9.4 33.9 0l47 47 47-47c9.4-9.4 24.6-9.4 33.9 0s9.4 24.6 0 33.9l-47 47 47 47c9.4 9.4 9.4 24.6 0 33.9s-24.6 9.4-33.9 0l-47-47-47 47c-9.4 9.4-24.6 9.4-33.9 0s-9.4-24.6 0-33.9l47-47-47-47c-9.4-9.4-9.4-24.6 0-33.9z"/></svg>';
    newTag.textContent = value;
    newTag.appendChild(closeBtn);
    if(!currentTags.includes(value)) {
      currentTags.push(value);
      targetTags.parentNode.insertBefore(newTag, targetTags);
      targetTags.value = '';
      tagsInputAutocomplete.textContent = '';
      targetTags.focus();
      closeBtn.addEventListener('click', () => removeTag(newTag, value));
    }
  }

  /**
   * Permet d'afficher et de remplir l'autocomplete
   *
   * @param value
   */
  const displayAutocomplete = async (value) => {
    tagsInputAutocomplete.textContent = '';
    let matchs = tags.filter(tag => tag.toLowerCase().includes(value.toLowerCase()));
    if(matchs.length > 0 && value.length > 0)
      matchs.sort().slice(0, 4).filter(match => !currentTags.includes(match)).forEach(match => {
        let div = document.createElement('div');
        div.addEventListener('click', async (e) => {
          await addTagToInput(e.target.textContent);
        })
        div.textContent = match;
        tagsInputAutocomplete.appendChild(div);
      })
  }

  /**
   * Permet d'ajouter ou de modifier un favori
   *
   * @returns {Promise<void>}
   */
  const insertOrUpdateBookMark = async () => {
    loaderContainer.style.display = 'flex';
    let objToUpdate = {
      title: targetTitle.value,
      url: targetUrl.value,
      favicon: targetFavicon.src,
      tags: currentTags,
      uid
    };
    await (currentDocId ? updateBookmark(currentDocId, objToUpdate) : await addBookmark(objToUpdate))
    loaderContainer.style.display = 'none';
    displayActivePage('end');
    currentDocId = null;
  }

  /**
   * Permet de récupérer les favoris d'un utilisateur et de les afficher
   *
   * @returns {Promise<void>}
   */
  const displayBookmarksList = async () => {
    listContainer.textContent = '';
    let bookmarks = await getBookmarks(uid);
    bookmarks.forEach(bookmark => {
      let link = document.createElement('a');
      link.classList.add('item-list');
      link.href = bookmark.url;
      link.target = '_blank';

      let img = document.createElement('img');
      img.src = bookmark.favicon;

      let title = document.createElement('div');
      title.classList.add('item-title');
      title.textContent = bookmark.title;

      let url = document.createElement('div');
      url.classList.add('item-url');
      url.textContent = bookmark.url;

      let rightContainer = document.createElement('div');

      rightContainer.appendChild(title);
      rightContainer.appendChild(url);

      link.appendChild(img);
      link.appendChild(rightContainer);

      listContainer.appendChild(link);
    })
    displayActivePage('list');
  }

  /**
   * Permet d'afficher la page active
   *
   * @param page
   */
  const displayActivePage = (page) => {
    if(page === 'form') {
      formContainer.style.display = 'block';
      listLink.style.display = 'initial';
      addLink.style.display = 'none';
    } else {
      formContainer.style.display = 'none';
    }

    if(page === 'end') {
      endContainer.style.display = 'flex';
      listLink.style.display = 'initial';
      addLink.style.display = 'none';
    } else {
      endContainer.style.display = 'none';
    }

    if(page === 'list') {
      listContainer.style.display = 'flex';
      listLink.style.display = 'none';
      addLink.style.display = 'initial';
    } else {
      listContainer.style.display = 'none';
    }

    if(page === 'signin') {
      signinContainer.style.display = 'block';
      listLink.style.visibility = 'hidden';
      listLink.style.display = 'initial';
      addLink.style.display = 'none';
      logoutLink.style.visibility = 'hidden';
    } else {
      signinContainer.style.display = 'none';
      listLink.style.visibility = 'initial';
      logoutLink.style.visibility = 'initial';
    }
  }

  /**
   * Permet d'écouter l'input tags pour ouvrir l'autocomplete
   */
  targetTags.addEventListener('input', async (e) => displayAutocomplete(e.target.value))

  /**
   * Permet d'écouter le clique sur le bouton de validation du formulaire
   */
  applyButton.addEventListener('click', async () => await insertOrUpdateBookMark());

  /**
   * Permet d'écouter le click pour la connexion
   */
  signinButton.addEventListener('click', async () => await signin(targetMail.value, targetPassword.value))

  /**
   * Permet d'écouter le click sur le lien pour la déconnexion
   */
  logoutLink.addEventListener('click', async () => await signout().then(() => init()))

  /**
   * Permet d'écouter le click sur le lien pour afficher la liste des favoris
   */
  listLink.addEventListener('click', async () => displayBookmarksList());

  /**
   * Permet d'écouter le click sur le lien pour afficher le formulaire d'ajout
   */
  addLink.addEventListener('click', async () => init());

  /**
   * Permet d'écouter le click sur l'input des tags
   */
  targetTags.parentNode.addEventListener('click', () => targetTags.focus());

  init();
});
